package platformbuilders.desafio.config;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import platformbuilders.desafio.dto.PessoaDTOOut;
import platformbuilders.desafio.model.PessoaEntity;

import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

@Configuration
public class ModelMapperConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.addConverter(pessoaEntityToPessoaDTOOut());
        return modelMapper;
    }

    private Converter<PessoaEntity, PessoaDTOOut> pessoaEntityToPessoaDTOOut() {
        return (context) -> {
            PessoaDTOOut out = Optional
                                    .of(context.getDestination())
                                    .orElse(new PessoaDTOOut());
            PessoaEntity entity = context.getSource();
            out.setCpf(entity.getCpf());
            out.setNome(entity.getNome());
            out.setId(entity.getId());

            Period idade = Period.between(entity.getDataNasc(), LocalDate.now());
            out.setIdade(idade.getYears());

            return out;
        };
    }
}
