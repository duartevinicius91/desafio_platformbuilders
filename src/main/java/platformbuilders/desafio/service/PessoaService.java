package platformbuilders.desafio.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import platformbuilders.desafio.dto.PessoaDTOIn;
import platformbuilders.desafio.dto.PessoaDTOOut;
import platformbuilders.desafio.model.PessoaEntity;
import platformbuilders.desafio.repository.PessoaRepository;

@Service
public class PessoaService {
    private final PessoaRepository pessoaRepository;
    private final ModelMapper modelMapper;

    public PessoaService(PessoaRepository pessoaRepository, ModelMapper modelMapper) {
        this.pessoaRepository = pessoaRepository;
        this.modelMapper = modelMapper;
    }

    public PessoaDTOOut save(PessoaDTOIn pessoaDTOIn) {
        PessoaEntity pessoa = modelMapper.map(pessoaDTOIn, PessoaEntity.class);
        pessoa = pessoaRepository.save(pessoa);
        return modelMapper.map(pessoa, PessoaDTOOut.class);
    }
}
