package platformbuilders.desafio.controller.advice;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.CollectionUtils.isEmpty;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiError {
    private Instant timestamp;
    private Integer status;
    private String error;
    private List<String> messages;
    private String path;

    public List<String> getMessages() {
        if (isEmpty(messages)) {
            messages = new ArrayList<>();
        }
        return messages;
    }
}
