package platformbuilders.desafio.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import platformbuilders.desafio.dto.PessoaDTOIn;
import platformbuilders.desafio.dto.PessoaDTOOut;
import platformbuilders.desafio.service.PessoaService;

import javax.validation.Valid;

@RestController
@RequestMapping("/pessoa")
public class PessoaController {
    private final PessoaService pessoaService;

    public PessoaController(PessoaService pessoaService) {
        this.pessoaService = pessoaService;
    }

    @PostMapping
    public ResponseEntity add(@Valid @RequestBody PessoaDTOIn pessoaDTOIn) {
        PessoaDTOOut pessoaDTOOut = pessoaService.save(pessoaDTOIn);

        return ResponseEntity.created(
                ServletUriComponentsBuilder
                        .fromCurrentServletMapping()
                        .path("/pessoa/{id}")
                        .build()
                        .expand(pessoaDTOOut.getId())
                        .toUri()
        ).build();

    }
}
