package platformbuilders.desafio.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@Entity
public class PessoaEntity {
    @Id
    @GeneratedValue
    private Long id;
    private LocalDate dataNasc;
    private String cpf;
    private String nome;
}
