package platformbuilders.desafio.dto;

import lombok.Data;

@Data
public class PessoaDTOOut {
    private Long id;
    private Integer idade;
    private String cpf;
    private String nome;
}
