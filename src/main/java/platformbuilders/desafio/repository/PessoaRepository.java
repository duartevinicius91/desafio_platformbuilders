package platformbuilders.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import platformbuilders.desafio.model.PessoaEntity;

public interface PessoaRepository extends JpaRepository<PessoaEntity, Long> {
}
