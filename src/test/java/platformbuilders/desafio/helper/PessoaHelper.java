package platformbuilders.desafio.helper;

import net.bytebuddy.asm.Advice;
import platformbuilders.desafio.dto.PessoaDTOIn;
import platformbuilders.desafio.dto.PessoaDTOOut;

import java.time.LocalDate;
import java.time.Period;

public class PessoaHelper {
    public static final LocalDate DATA_NASC = LocalDate.of(2020, 12, 10);
    public static final String CPF = "305.568.820-16";
    public static final String NOME = "JOAO SILVA";

    public static PessoaDTOIn pessoaDTOIn() {
        PessoaDTOIn pessoaDTOIn = new PessoaDTOIn();
        pessoaDTOIn.setCpf(CPF);
        pessoaDTOIn.setNome(NOME);
        pessoaDTOIn.setDataNasc(DATA_NASC);
        return pessoaDTOIn;
    }

    public static PessoaDTOOut pessoaDTOOut() {
        PessoaDTOOut pessoaDTOOut = new PessoaDTOOut();
        pessoaDTOOut.setCpf(CPF);
        pessoaDTOOut.setNome(NOME);
        pessoaDTOOut.setIdade(Period.between(LocalDate.now(), DATA_NASC).getYears());
        return pessoaDTOOut;
    }
}
