package platformbuilders.desafio.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import platformbuilders.desafio.dto.PessoaDTOIn;
import platformbuilders.desafio.dto.PessoaDTOOut;
import platformbuilders.desafio.helper.PessoaHelper;
import platformbuilders.desafio.model.PessoaEntity;
import platformbuilders.desafio.service.PessoaService;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(PessoaController.class)
class PessoaControllerTest {
    @Autowired
    private MockMvc mvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private PessoaService service;

    private static String ENDPOINT = "/pessoa";

    @Test
    @DisplayName("Ao solicitar a adição de pessoa deve retornar status 201")
    public void teste_adicao_pessoa() throws Exception {
        PessoaDTOIn alex = PessoaHelper.pessoaDTOIn();

        given(service.save(any(PessoaDTOIn.class)))
                .willReturn(PessoaHelper.pessoaDTOOut());

        mvc.perform(doPost(alex))
                .andExpect(status().isCreated());
    }

    private MockHttpServletRequestBuilder doPost(PessoaDTOIn pessoa) throws JsonProcessingException {
        return post(ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(pessoa));
    }
}