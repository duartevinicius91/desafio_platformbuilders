Pessoa Desenvolvedora Backend Java Sênior - Cód 158 (Remoto)
Buscamos Pessoas DesenvolvedorAs apaixonadAs pela linguagem Java, inconformados por natureza e encantados em gerar valor.

Na Builders você irá fazer parte de um time de empreendedores, onde buscamos inovação com disrupção de forma significativa, com qualidade e simplicidade - seja através do código ou através da melhoria contínua alinhada à performance.

Tá afim de iniciar essa jornada com a gente, gerando transformação de forma impactante? Se liga nos detalhes abaixo:

#SejaBuilder



Tecnologias que vão gerar valor nos projetos:


Spring Framework
Microservices
REST
Design Patterns
DDD

O que te aproxima de ser um (a) Builder:

Kubernetes
Jenkins
Maven
Docker



INFORMAÇÕES ADICIONAIS

Oportunidades reais de empreender e impactar
Fazer parte de uma comunidade de empreendedores de ponta, o Movimento
Inovação e criação liberados
Participar de um processo único de geração de valor
Contribuir na criação de métodos disruptivos
Colocar suas ideias no mercado, na prática! :)
Possibilidade de Profit share em novos Negócios
Prestador de Serviços (PJ)
Flexibilidade de horário